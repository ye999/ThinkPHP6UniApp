-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.7.25 - MySQL Community Server (GPL)
-- 服务器OS:                        Win64
-- HeidiSQL 版本:                  10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table yifeng.yf_classitem
CREATE TABLE IF NOT EXISTS `yf_classitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classid` int(10) NOT NULL DEFAULT '0' COMMENT '价格类型id',
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '配置项名称',
  `sort` int(10) NOT NULL DEFAULT '100' COMMENT '排序',
  `value` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT '值/价格',
  `nvalue` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT '值/价格',
  `formtype` int(10) NOT NULL DEFAULT '100' COMMENT '表单类型，1：单行，2：多行',
  `zhangfu` decimal(10,4) DEFAULT '0.0000' COMMENT '涨幅',
  `nzhangfu` decimal(10,4) DEFAULT '0.0000' COMMENT '涨幅',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table yifeng.yf_classs
CREATE TABLE IF NOT EXISTS `yf_classs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '价格类型',
  `label` varchar(60) NOT NULL DEFAULT '',
  `sort` int(10) NOT NULL DEFAULT '100' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `label` (`label`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table yifeng.yf_manager
CREATE TABLE IF NOT EXISTS `yf_manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL DEFAULT '' COMMENT '用户名，登陆使用',
  `password` varchar(100) NOT NULL DEFAULT '$2y$10$M5tuAOy3kV.cE3tSKhKhe.aVAuzxKysKt4inRoFE0b9cZG.RUwtpC' COMMENT '用户密码',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table yifeng.yf_migrations
CREATE TABLE IF NOT EXISTS `yf_migrations` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table yifeng.yf_users
CREATE TABLE IF NOT EXISTS `yf_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL DEFAULT '' COMMENT '用户名，登陆使用',
  `password` varchar(80) NOT NULL DEFAULT 'e10adc3949ba59abbe56e057f20f883e' COMMENT '用户密码',
  `login_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '登陆状态',
  `level` tinyint(1) NOT NULL DEFAULT '0' COMMENT '会员级别,0:普通会员，1：员工',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '账号状态',
  `login_code` varchar(32) NOT NULL DEFAULT '0' COMMENT '排他性登陆标识',
  `expiration_time` int(11) NOT NULL DEFAULT '0' COMMENT '过期时间',
  `last_login_ip` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `last_login_time` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除状态，1已删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
